# -*- coding: utf-8 -*-
# Copyright (C) 2020  James Alexander Clark <james.clark@ligo.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Utilities for data registration.
"""

import errno
import logging
# Native
import os
from time import sleep

from datetime import datetime
import subprocess
import paramiko
import socket
import configparser

try:
    from typing import TypedDict
except ImportError:
    from typing_extensions import TypedDict

import gfal2

from rucio.client.client import Client
from .diskcache import DiskCacheFile

from rucio.rse.protocols.protocol import RSEDeterministicTranslation
from rucio.common.exception import RucioException

logging.getLogger("gfal2").setLevel(logging.WARNING)

SUCCESS = 0
FAILURE = 1


class DiskCacheParams(TypedDict):
    """Dictionary describing the span and type of data we're interested in
    """
    path: str
    minimum_gps: int
    maximum_gps: int
    regexp: str
    extension: str


class ExecOpts(TypedDict):
    """Dictionary of process execution options for registration
    """
    nthreads: int
    sleep_interval: int
    online: bool
    save_state: bool


class RSEOpts(TypedDict):
    """Dictionary of RSE info
    """
    rse: str
    client_domain: str


class FrameChkOpts(TypedDict):
    """Dictionary of frame check options
    """
    disable: bool
    executable: str
    exe_opts: str
    username: str
    hostname: str
    key_filename: str
    timeout: int
    banner_timeout: int
    auth_timeout: int
    channel_timeout: int
    exec_timeout: int
    retries: int
    sleep_time: int


class DidMeta(TypedDict):
    """Dictionary of mutable DID-level science metadata"""
    events: int
    guid: str
    project: str
    datatype: str
    run_number: int
    stream_name: str
    prod_step: str
    version: str
    campaign: str
    task_id: int
    panda_id: int
    lumiblocknr: int
    provenance: str # 2 character limit
    phys_group: str


def to_dict(config: configparser.ConfigParser) -> dict:
    """
    function converts a ConfigParser structure into a nested dict
    Each section name is a first level key in the the dict, and the key values of the section
    becomes the dict in the second level
    {
        'section_name': {
            'key': 'value'
        }
    }
    :param config:  the ConfigParser with the file already loaded
    :return: a nested dict
    """
    return {section_name: dict(config[section_name]) for section_name in config.sections()}


# FIXME if we have more of these, should move to their own exception.py
class FrameValidationFailure(RucioException):
    """
    Failure in frame file validation from e.g. framecpp_verify or FrCheck
    """

    def __init__(self, error_code=255, *args, **kwargs):
        super(FrameValidationFailure, self).__init__(*args, **kwargs)
        self._message = f"Frame validation failure: exit code {error_code}"
        self.error_code = error_code


class KeepalivesFilter(object):
    @staticmethod
    def filter(record):
        return record.msg.find('keepalive@openssh.com') < 0


def read_diskcache(diskcache_dump, regexp, **kwargs):
    """
    Read ascii dump from ldas diskcache

    """
    try:
        os.stat(diskcache_dump)
    except (IOError, OSError) as cache_error:
        logging.critical(cache_error)
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT),
                                diskcache_dump) from cache_error

    # Parse kwargs
    if 'extension' in kwargs:
        extension = kwargs['extension']
    else:
        logging.info("No extension provided, using .gwf")
        extension = 'gwf'

    if 'minimum_gps' in kwargs:
        minimum_gps = kwargs['minimum_gps']
    else:
        logging.info("No minimum_gps provided, using 0")
        minimum_gps = 0

    if 'maximum_gps' in kwargs:
        maximum_gps = kwargs['maximum_gps']
    else:
        logging.info("No maximum_gps provided, using 2000000000")
        maximum_gps = 2000000000

    logging.info("Parsing diskcache %s", diskcache_dump)
    logging.info("regexp=%s, gps-start-time=%d, gps-end-time=%d", regexp,
                 minimum_gps, maximum_gps)

    diskcache = DiskCacheFile(diskcache_dump, minimum_gps=minimum_gps,
                              maximum_gps=maximum_gps, regexp=regexp,
                              extension=extension, prune=True,
                              update_file_count=True)
    pathlist = list(diskcache.expand())

    logging.info(f"Found %d matching files in diskcache dump (%s)", len(pathlist), diskcache_dump)

    return pathlist


def gps_end_of_list(lfns: list) -> int:
    """
    Get the GPS end time covered by a list of frame LFNs

    Parameters
    ----------
    lfns : `list`
        List of LFNs

    Returns
    -------
    gps_end_time : `int`
        End time of last file in the list

    """

    last_file = os.path.basename(sorted(lfns)[-1])
    gps_start = int(last_file.split('-')[2])
    duration = int(os.path.splitext(last_file.split('-')[3])[0])

    return gps_start + duration


def files_to_register(scope: str, diskcache_dump: str, frame_type: str, **kwargs: object) -> list:
    """
    Find files present in diskcache ASCII dump which are not present in the rucio catalog.

    Parameters
    ----------
    scope : `str`
        Rucio namespace scope for datasets of this frame type.
    diskcache_dump: `str`
        Path to ASCII dump from LDAS diskcache or pmdc
    frame_type: `str`
        Frame file type regex.  Must be unique; e.g.: `H1_R-` vs `H1_RDS`
    key client : `rucio.client.client.Client`
        Rucio client instance.  Will be created if not specified.
    key extension : `str`
        File extension.  Default: `.gwf`
    key minimum_gps : `int`
        Minimum GPS time for frame files parsed from diskcache.  Default: 0
    key maximum_gps : `int`
        Maximum GPS time for frame files parsed from diskcache.  Default: 2000000000
    """

    # --- Rucio catalog

    # Instantiate rucio client
    if 'client' in kwargs:
        client = kwargs['client']
    else:
        client = Client()

    # Get list of all pre-registered and attached files
    logging.info("Finding registered, attached files matching pattern '*%s*'", frame_type)
    logging.debug("Listing all datasets matching pattern '*%s*'", frame_type)
    datasets = sorted(list(client.list_dids(scope=scope, filters={'name': f'*{frame_type}*',
                                                                  'type': 'dataset'})))
    logging.debug("Found %d datasets matching pattern '*%s*'", len(datasets), frame_type)

    # Filter out datasets which *cannot* include this minimum/maximum gps time
    if 'minimum_gps' in kwargs:
        dataset_times = {dataset: int('{:<010s}'.format(dataset.split('-')[-1])) for dataset in
                         datasets}
        # Go up until the oldest dataset that could still include this time
        old_datasets = [dataset for dataset in datasets \
                        if dataset_times[dataset] < kwargs['minimum_gps']][:-1]
        _ = [datasets.remove(dataset) for dataset in old_datasets]

        logging.debug("Reduced to %d datasets with GPS>%d", len(datasets), kwargs['minimum_gps'])

    if 'maximum_gps' in kwargs:
        new_datasets = [dataset for dataset in datasets \
                        if dataset_times[dataset] > kwargs['maximum_gps']]
        _ = [datasets.remove(dataset) for dataset in new_datasets]

        logging.debug("Reduced again to %d datasets with GPS<%d", len(datasets),
                      kwargs['maximum_gps'])

    logging.debug("Using %d datasets: %s", len(datasets), str(datasets))

    # Remove unattached files from rucio_files
    logging.debug("Compiling list of attached files from each dataset")
    attached_files = list()
    for dataset in datasets:
        attached_files.extend(list(client.list_content(scope=scope, name=dataset)))
    attached_files = [attached_file['name'] for attached_file in attached_files]
    logging.info("Found %d attached files matching pattern '*%s*'",
                 len(attached_files), frame_type)

    # --- Diskcache
    # Get files to register from diskcache ascii dump
    try:
        obs_run = scope.split('.')[-1]
        regex = f'(.*{obs_run}.*{frame_type})'
        file_paths = read_diskcache(diskcache_dump, regex, **kwargs)
    except ValueError:
        logging.warning("Diskcache read failed, parsing %s as PFN list",
                        diskcache_dump)
        with open(diskcache_dump, 'r', encoding="utf-8") as pfn_stream:
            file_paths = pfn_stream.read().splitlines()

    logging.debug("Comparing rucio catalog with diskcache dump")

    # Reduce to list of new-to-rucio LFNs
    diskcache_pfn_dict = {os.path.basename(file_path): file_path for file_path in file_paths}
    diskcache_lfns = [os.path.basename(file_path) for file_path in file_paths]
    new_lfns = list(set(diskcache_lfns).difference(attached_files))
    new_pfns = [diskcache_pfn_dict[new_lfn] for new_lfn in new_lfns]

    logging.info("Found %d NEW files (in diskcache but not present in rucio datasets)",
                 len(new_pfns))

    return new_pfns


def grouper(lst, nchunk):
    """
    Split a list into nchunks
    """
    for idx in range(0, nchunk):
        yield lst[idx::nchunk]


def chunker(lst, nchunk):
    """
    Split a list into chunks of size nchunk
    """
    for idx in range(0, len(lst), nchunk):
        yield lst[idx:idx + nchunk]


def gfal_md5(pfn, max_tries=5, sleep_time=5):
    """
    Compute the MD5 checksum of a file using GFAL2.

    """
    ctxt = gfal2.creat_context()
    for ntries in range(max_tries):
        try:
            return ctxt.checksum(pfn, 'MD5')
        except gfal2.GError as gfalerr:
            logging.error(gfalerr)
            logging.error("GFAL checksum failure (attempt %d of %d)", ntries + 1,
                          max_tries)
            logging.info("Sleeping for %d s", sleep_time)
            sleep(sleep_time)
            sleep_time *= 2

    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), pfn)


def gfal_adler32(pfn, max_tries=5, sleep_time=5):
    """
    Compute the Adler32 checksum of a file using GFAL2.

    """
    ctxt = gfal2.creat_context()
    for ntries in range(max_tries):
        try:
            return ctxt.checksum(pfn, 'Adler32')
        except gfal2.GError as gfalerr:
            logging.error(gfalerr)
            logging.error("GFAL checksum failure (attempt %d of %d)", ntries + 1,
                          max_tries)
            logging.info("Sleeping for %d s", sleep_time)
            sleep(sleep_time)
            sleep_time *= 2

    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), pfn)


def gfal_stat(pfn, max_tries=5, sleep_time=5):
    """
    Stat a file.
    """
    ctxt = gfal2.creat_context()
    for ntries in range(max_tries):
        try:
            return ctxt.stat(pfn)
        except gfal2.GError as gfalerr:
            logging.error(gfalerr)
            logging.error("GFAL stat failure (attempt %d of %d)", ntries + 1,
                          max_tries)
            logging.info("Sleeping for %d s", sleep_time)
            sleep(sleep_time)
            sleep_time *= 2

    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), pfn)


def gfal_bytes(pfn):
    """
    Compute the size in bytes of a file.

    """
    return gfal_stat(pfn).st_size


def validate_frame(frame_file_path: str, framechk_exe: str, framechk_opts: str, **kwargs) -> tuple:
    """Validate a frame file by running an external application such as FrCheck or
    framecpp_verify.

    If ssh configurations are provided in kwargs, will attempt execution over ssh via paramiko.
    
    Parameters
    ----------
    frame_file_path : `str`
        Path the the frame file to validate.
    framechk_exe : `str`
        The executable used to validate the frame file
    framechk_opts : `str`
        Command line options for `framechk_exe`

    :Keyword Arguments:
    * *hostname* (``str``) --
      Hostname for ssh server
    * *key_filename* (``str``) --
      Path to private ssh key file
    * *username* (``str``) --
      ssh username

    Returns
    -------
    (exit_code, stdout, current UTC time) : `tuple`
        A tuple with the exit code and stdout for the frame validation application
    """

    if 'hostname' in kwargs and kwargs['hostname'] != '' or None:
        # Execute via paramiko

        # Check all kwargs defined for ssh
        try:
            # Set non-optional ssh kwargs
            hostname = kwargs['hostname']
            username = kwargs['username']
            key_filename = kwargs['key_filename']

        except KeyError as ssh_check:
            raise KeyError(f"Missing SSH option") from ssh_check

        if 'retries' in kwargs:
            retries = kwargs['retries']
        else:
            retries = 5
        if 'sleep_time' in kwargs:
            sleep_time = kwargs['sleep_time']
        else:
            sleep_time = 5
        if 'exec_timeout' in kwargs:
            exec_timeout = kwargs['exec_timeout']

        logging.debug("Paramiko using kwargs: %s", str(kwargs))

        # reduce kwargs to optional paramiko connect args like timeouts
        kwargs = {key: value for key, value in kwargs.items() if
                  key not in ['hostname', 'username', 'key_filename', 'retries']}

        # Now reduce to supported timeout values
        kwargs = {key: value for key, value in kwargs.items() if
                  key in ['timeout', 'banner_timeout', 'auth_timeout',
                          'channel_timeout']}

        policy = paramiko.AutoAddPolicy()

        # noinspection PyUnresolvedReferences
        paramiko.util.get_logger('paramiko.transport').addFilter(KeepalivesFilter())

        frame_validation_cmd = f"{framechk_exe} {framechk_opts} {frame_file_path}"

        # Add a retry/backoff loop
        for ntries in range(retries):
            try:
                with paramiko.SSHClient() as ssh_client:
                    ssh_client.set_missing_host_key_policy(policy)
                    ssh_client.connect(hostname=hostname, username=username,
                                       key_filename=key_filename,
                                       **kwargs)
                    _, _stdout, _ = ssh_client.exec_command(command=frame_validation_cmd,
                                                            timeout=exec_timeout)
                    stdout = _stdout.read().decode()
                    exit_code = _stdout.channel.recv_exit_status()
                break
            except (paramiko.ssh_exception.SSHException, socket.error, socket.error, TimeoutError) as \
                    paramiko_exc:
                logging.error(paramiko_exc)
                if ntries == retries-1:
                    logging.error("Ran out of SSH retries")
                    raise paramiko.ssh_exception.SSHException(paramiko_exc)
                logging.error("SSH failure (attempt %d of %d)", ntries+1, retries)
                logging.info("Sleeping for %d s", sleep_time)
                sleep(sleep_time)
                sleep_time *= 2

    else:
        # Execute as a subprocess locally
        cmds_list = [framechk_exe] + framechk_opts.split() + [frame_file_path]
        result = subprocess.run(cmds_list, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                                universal_newlines=True)
        stdout = result.stdout
        exit_code = result.returncode

    if exit_code != 0:
        raise FrameValidationFailure(exit_code)

    return exit_code, stdout, str(datetime.utcnow())


def get_pfn(rse_info, scope, path, force_scheme=None):
    """
    Return the full PFN with URL at this RSE

    """

    # Strip out any double //
    path = path.replace('//', '/')

    # Determine URI prefix from RSE configuration
    protocol = None
    if force_scheme:
        protocol = next(protocol for protocol in rse_info['protocols']
                        if protocol['scheme'] == force_scheme)

        # CHeck protocol supports WAN I/O
        # XXX WHY??
        #if not protocol['domains']['wan'].items() <= {'read': 1, 'write': 1, 'delete': 1}.items():
        #    protocol = None

    else:
        # Identfiy the first protocol with WAN I/O
        for protocol in rse_info['protocols']:
            if {'read': 1, 'write': 1, 'delete': 1}.items() <= protocol['domains']['wan'].items():
                break
            else:
                protocol = None

    if protocol is None:
        raise RucioException(f"RSE {rse_info['rse']} does not support WAN domain I/O")

    scheme = protocol['scheme']
    prefix = protocol['prefix']
    port = protocol['port']
    hostname = protocol['hostname']

    # Confirm WAN I/O

    if rse_info['deterministic']:
        # Use the lfn2pfn algorithm at this RSE
        lfn2pfn_translator = RSEDeterministicTranslation(rse=rse_info['rse'])
        path = prefix + '/' + \
               lfn2pfn_translator.path(scope=scope, name=os.path.basename(path))

    url = scheme + '://' + hostname
    if port != 0:
        url += ':' + str(port)

    # Special cases
    if rse_info['rse'] in ['ICRR-STAGING']:
        path = path.replace('/gpfs', '')

    url += path

    return url
