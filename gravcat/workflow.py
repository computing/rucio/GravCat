# -*- coding: utf-8 -*-
# Copyright (C) 2022  James Alexander Clark <james.clark@ligo.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Utility to identify and generate k8s/HTCondor jobs to populate gaps in rucio catalog.  May also be
used for offline bulk registration.
"""

import os
import sys
import logging
from pathlib import Path
import shutil
from distutils.spawn import find_executable

import htcondor
from htcondor import dags
import classad

from rucio.common.config import config_get

from . import utils

__author__ = 'James Alexander Clark <james.clark@ligo.org>'

logging.basicConfig(stream=sys.stdout, level=getattr(logging, config_get('common', 'loglevel',
                                                                         raise_exception=False,
                                                                         default='DEBUG').upper()),
                    format='%(asctime)s\t%(process)d\t%(levelname)s\t%(message)s')

GRAVCAT_ARGS = "--config-file {CONFIG_FILE} --pfns-file {PFNS_FILE} --rse {RSE} "\
               "--client-domain {CLIENT_DOMAIN} register"


def check_gaps(scope: str, disk_cache_params: utils.DiskCacheParams, n_pfns_per_job: int = 10,
               pfn_list_dir: str = 'new-pfn-lists') -> list:
    """

    Parameters
    ----------
    scope : `str`
            Rucio scope in which files will be registered.
    disk_cache_params: `gravcat.utils.DiskCacheParams`
            Typed dict with params to parse diskcache ASCII dump.
    n_pfns_per_job

    Returns
    -------
    object

    """

    ## Set up output directory for PFN lists
    if os.path.isdir(pfn_list_dir):
        logging.warning("Removing existing PFN list dir: %s", pfn_list_dir)
        shutil.rmtree(pfn_list_dir, ignore_errors=True)
    Path(pfn_list_dir).mkdir(parents=True)

    # Find missing PFNs
    new_pfns = utils.files_to_register(scope=scope, diskcache_dump=disk_cache_params['path'],
                                       frame_type=disk_cache_params['regexp'],
                                       minimum_gps=disk_cache_params['minimum_gps'],
                                       maximum_gps=disk_cache_params['maximum_gps'],
                                       extension=disk_cache_params['extension'])

    pfn_lists = []
    if new_pfns:
        logging.info("Writing %d NEW PFNs to file.", len(new_pfns))

        # Split into groups
        pfn_groups = utils.chunker(new_pfns, n_pfns_per_job)

        # Write PFN groups to file
        pfn_list_prefix = f"{scope}:{disk_cache_params['regexp'].strip('-')}"
        logging.debug("Writing new PFN lists to %s-*.txt", pfn_list_prefix)

        # Store PFN list filenames to use in jobs later
        pfn_lists = []
        for pfn_group in pfn_groups:

            gps_start = int(os.path.basename(pfn_group[0]).split('-')[2])
            gps_end = int(os.path.basename(pfn_group[-1]).split('-')[2]) + \
                      int(os.path.splitext(os.path.basename(pfn_group[-1]))[0].split('-')[3])

            this_file_name = os.path.abspath(os.path.join(pfn_list_dir,
                                          f"{pfn_list_prefix}-{gps_start}_{gps_end}.txt"))
            pfn_lists.append(f"{this_file_name}")

            with open(f"{this_file_name}", 'w', encoding='UTF-8') as pfn_list_file:
                for pfn in pfn_group:
                    pfn_list_file.write(f"{pfn}\n")

        logging.info("Written new PFNs to %d files", len(pfn_lists))

    return pfn_lists


def make_register_job(config_file: str, rse: str, htcondor_overrides: dict = None,
                      client_domain: str = "lan",
                      file_list: str = "$(file_list_pfn)") -> htcondor.Submit:
    """

    Parameters
    ----------
    config_file
    rse
    htcondor_overrides
    client_domain
    file_list

    Returns
    -------
    object

    """
    # Populate with defaults

    if htcondor_overrides is None:
        htcondor_overrides = {}

    gravcat_args = GRAVCAT_ARGS.format(CONFIG_FILE=os.path.abspath(config_file), RSE=rse,
                                       PFNS_FILE=file_list, CLIENT_DOMAIN=client_domain)

    register_job_dict = {
        "universe": "vanilla",
        "executable": find_executable(sys.argv[0]),
        "arguments": gravcat_args,
        "log": "gravcat-register-$(cluster)-$(process).log",
        "error": "gravcat-register-$(cluster)-$(process).err",
        "output": "gravcat-register-$(cluster)-$(process).out",
        "getenv": "RUCIO_HOME",
        "request_cpus": "1",
        "request_memory": "1GB",
        "request_disk": "1GB",
        "batch_name": "gravcat-register-$(cluster)-$(process)",}

    # Apply overrides
    register_job_dict.update(htcondor_overrides)

    # Set classad quotes for any fields we know should have them
    for field in ['singularity-image']:
        if field not in register_job_dict.keys():
            continue
        else:
            register_job_dict[field] = classad.quote(os.path.abspath(register_job_dict[field]))

    # Now rename singularity image field
    if 'singularity-image' in register_job_dict.keys():
        register_job_dict['MY.SingularityImage'] = register_job_dict.pop('singularity-image')
        register_job_dict['requirements'] = "HAS_SINGULARITY=?=TRUE" # FIXME append requirement
        register_job_dict['transfer_executable']  = "False"
        
    return htcondor.Submit(register_job_dict)


def make_register_dag(file_lists: list, config_file: str, rse: str,
                      htcondor_overrides: dict=None, client_domain: str = "lan",
                      **kwargs) -> str:

    # Standard defaults
    use_kwargs = {'n_retries': 3, 'dag_dir': './', 'dag_tag': '0'}
    use_kwargs.update(kwargs)

    dag_dir = os.path.abspath(os.path.join(use_kwargs['dag_dir'],
                                           f"gravcat-register-condor-{use_kwargs['dag_tag']}"))

    ## Job, workflow descriptions
    register_job_description = make_register_job(config_file, rse, htcondor_overrides,
                                                 client_domain, '$(file_list_pfn)')
    # Put userlogs, stdout/err in dagdir
    for field in ['log', 'error', 'output']:
        register_job_description[field] = os.path.join(dag_dir, register_job_description[field])

    logging.info("Submit file job description:\n\n%s", register_job_description)

    ## Generate inputs
    pfn_files_dicts = [{'file_list_pfn': file_list} for file_list in file_lists]

    # Create DAG
    dag = dags.DAG()

    # Add dag layer for registration job
    _ = dag.layer(
        name=f"gravcat-register-{use_kwargs['dag_tag']}",
        submit_description=register_job_description,
        vars=pfn_files_dicts,
        retries=use_kwargs['n_retries'])

    ## Write workflow to disk
    if os.path.isdir(dag_dir):
        logging.warning("Removing existing DAG dir: %s", dag_dir)
        shutil.rmtree(dag_dir, ignore_errors=True)

    # make the magic happen!
    dag_file = htcondor.dags.write_dag(dag=dag, dag_dir=dag_dir,
                                       dag_file_name=f"gravcat-register-{use_kwargs['dag_tag']}.dag")

    logging.info("DAGMan file description:\n\n%s\n\n", dag.describe())
    logging.info("DAG directory: %s", dag_dir)
    logging.info("DAG description file: %s", dag_file)
    logging.info("To submit, exit container runtime if necessary and:\n\nexport RUCIO_HOME=%s && "
                 "pushd %s && condor_submit_dag -include_env RUCIO_HOME %s && popd\n\n",
                 os.environ['RUCIO_HOME'], dag_dir, dag_file)
