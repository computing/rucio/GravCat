# -*- coding: utf-8 -*-
# Copyright (C) 2020  James Alexander Clark <james.clark@ligo.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Utilities for registering IGWN files already on storage
"""

import logging
import os
import random
import re
import sys
import tempfile
import threading
import time
from urllib.parse import urlparse

from requests.exceptions import ConnectionError

# python3.6 compatibility
try:
    from typing import TypedDict
except ImportError:
    from typing_extensions import TypedDict
from typing import List, Tuple, Set

import rucio.rse.rsemanager as rsemgr
from rucio.client.client import Client
from rucio.common.config import config_get
from rucio.common.exception import (CannotAuthenticate, RucioException, UnsupportedOperation, DataIdentifierAlreadyExists, DuplicateContent)
from rucio.common.utils import extract_scope
from rucio.common.utils import daemon_sleep

from gravcat_policy import lfn2pfn
from . import utils

# Maximum number of replica dicts to send to the server
_MAX_CHUNK = 1000

logging.basicConfig(stream=sys.stdout, level=getattr(logging, config_get('common', 'loglevel',
                                                                         raise_exception=False,
                                                                         default='DEBUG').upper()),
                    format='%(asctime)s\t%(process)d\t%(levelname)s\t%(message)s')

# Force paramiko log level
logging.getLogger("paramiko").setLevel(logging.INFO)

GRACEFUL_STOP = threading.Event()


def stop():
    """Graceful exit with `threading` module.
    """
    threading.Event().set()


def get_state_dir():
    try:
        state_dir = os.environ['GRAVCAT_STATE_DIR']
    except KeyError:
        state_dir = tempfile.gettempdir()
    return state_dir


def get_dataset_name(name: str):
    """Extract the appropriate dataset name from the LDAS filename:
    `<obs>-<content>-<gps_prefix>`.
    """
    # TODO the gps_prefix will depend on the data type so let's get this from a function in the
    #  lfn2pfn package
    obs, content, gps_start, _, _ = lfn2pfn._LDAS_LFN_RE.match(name).groups()
    ldas_conventions = lfn2pfn.lookup_ldas_conventions(f"{obs}1", content)
    gps_prefix = gps_start[:ldas_conventions["gps_truncation"]]

    return f"{obs}-{content}-{gps_prefix}"


def set_status(scope: str, name: str, open_status: bool, _client: Client = None) -> None:
    """
    Safely set the status of a did to open or closed.  Modifies the status of the dataset in-place.

    Parameters
    ----------
    scope : `str`
        Scope for dataset.
    name : `str`
        Name of dataset.
    open_status : `bool`
        Open (true) or closed (false).
    _client : `rucio.client.client.Client`
        Rucio client instance.  Will be created if needed.

    """
    client = _client if _client else Client()
    try:
        logging.debug("Setting DID %s:%s open_status=%s", scope, name, open_status)
        client.set_status(scope=scope, name=name, open=open_status)
    except UnsupportedOperation:
        logging.debug("DID %s:%s already has open=%s", scope, name, open_status)
    except RucioException as set_status_exc:
        logging.exception(set_status_exc)
        raise RuntimeError(f"Error setting DID {scope}:{name} open_status"
                           f"={open_status}") from set_status_exc


class RegisterClient:
    """Parent class to enumerate data for registration in Rucio catalog.
    """

    def __init__(self, pathlist: List[str], scope: str, rse_opts: utils.RSEOpts,
                 common_metadata: dict = None, _client: Client = None) -> None:
        """Instantiate registration client.

        Parameters
        ----------
        pathlist : `list[str]`
            List of files to register.
        scope : `str`
            Rucio scope in which files will be registered.
        rse_opts : `utils.RSEOpts`
            Name and scheme to use for RSE
        common_metadata: `dict`
            Dictionary of key-value pairs which will be attached as JSON metadata to all DIDs.
        _client : `rucio.client.client.Client`
            Optionally specify an existing Rucio client instance to connect to server.
        """

        # Instantiate the client connection
        self.client = _client if _client else Client()

        self.scope = scope
        self.rse_opts = rse_opts

        # Separate out JSON and mutable DID metadata
        self.did_metadata = self.extract_did_metadata(common_metadata)

        self.json_metadata = self.extract_json_metadata(common_metadata)

        # List of replica dicts
        self.replicas = self.replica_list(pathlist)

    @property
    def scope(self):
        """Return Scope replicas registered at"""
        return self._scope

    @scope.setter
    def scope(self, scope: str):
        """Set scope for registration

        Parameters
        ----------
        scope : `str`
        """
        if scope not in (vscope for vscope in self.client.list_scopes()):
            raise ValueError(f"Scope {scope} does not exist")
        self._scope = scope

    @property
    def rse_opts(self):
        """Return RSE replicas registered at."""
        return self._rse_opts

    @rse_opts.setter
    def rse_opts(self, rse_opts: utils.RSEOpts):
        """Set RSE for registration.

        Parameters
        ----------
        rse_opts : `utils.RSEOpts`
            Name and scheme to use for RSE
        """
        if rse_opts['rse'] not in [vrse['rse'] for vrse in self.client.list_rses()]:
            raise RucioException(f"RSE {rse_opts['rse']} does not exist")

        self.rse_info = rsemgr.get_rse_info(rse_opts['rse'])
#       # FIXME check which domains RSE supports
#       if rse_opts['client_domain'] not in ['wan', 'lan', 'all']:
#           raise RucioException(f"{rse_opts['rse']} does not support domain:"
#                                f" {rse_opts['client_domain']}")

        self._rse_opts = rse_opts

    @property
    def did_metadata(self):
        """Mutable DID metadata"""
        return self._did_metadata

    @did_metadata.setter
    def did_metadata(self, did_metadata: utils.DidMeta):
        """Set Mutable DID metadata from any supported fields in common_metadata"""
        self._did_metadata = did_metadata

    @staticmethod
    def extract_did_metadata(common_metadata: dict):
        """Get Mutable DID metadata from any supported fields in common_metadata"""

        # Get these keys from the DidMeta typed dict so we only have one definition to maintain
        did_keys = utils.DidMeta.__annotations__.keys()

        did_metadata = {key: value for key, value in common_metadata.items() if key in
                              did_keys}

        return did_metadata

    @property
    def json_metadata(self):
        """Mutable DID metadata"""
        return self._json_metadata

    @json_metadata.setter
    def json_metadata(self, json_metadata: dict):
        """Set Mutable DID metadata from any supported fields in common_metadata"""
        self._json_metadata = json_metadata

    @staticmethod
    def extract_json_metadata(common_metadata: dict):
        """Get Mutable JSON metadata from any remaining fields in common_metadata which are not
        supported by DID-level metadata"""

        # Get these keys from the DidMeta typed dict so we only have one definition to maintain
        did_keys = utils.DidMeta.__annotations__.keys()

        json_metadata = {key: value for key, value in common_metadata.items() if key not in
                         did_keys}

        return json_metadata

    @property
    def replicas(self):
        """Return the replica list
        """
        return self._replicas

    @replicas.setter
    def replicas(self, replicas: List[dict]):
        """"""
        self._replicas = replicas

    @property
    def size(self):
        """Get size of dataset."""
        return len(self.replicas)

    def replica_list(self, pathlist: List[str]) -> List[dict]:
        """Create a list of dictionaries of DID metadata

        Parameters
        ----------
        pathlist : `list[str]`
            List of files to register.

        Returns
        -------
        replicas : `list[dict]`
            A list of replica dictionaries.

        """

        def _get_extra_json_meta(path: str) -> dict:
            """Create a dictionary of IGWN metadata from a filename whose format follows
            https://dcc.ligo.org/LIGO-T010150.

            Parameters
            ----------
            path : `str`
                Path or basename of a file.

            Returns
            -------
            metadata : `dict`
                A dictionary of metadata parsed from an LDAS filename and augmented with any
                additional key-value pairs in `common_metadata` in the parent function.
            """

            metadata = {}
            if lfn2pfn._LDAS_LFN_RE.match(os.path.basename(path)):
                (metadata['obs'], metadata['content'], metadata['gps-start-time'],
                 metadata['duration'], metadata['extension']) = lfn2pfn._LDAS_LFN_RE.match(
                    os.path.basename(path)).groups()

                for meta_key in ['gps-start-time', 'duration']:
                    # noinspection PyUnresolvedReferences
                    metadata[meta_key] = int(metadata[meta_key])
                metadata['gps-end-time'] = metadata['gps-start-time'] + metadata['duration']

            if self.json_metadata:
                metadata.update(self.json_metadata)

            return metadata

        def _construct_lfn(pfn: str) -> str:
            """Construct LFN.

            Currently, just takes the basename of a PFN but could be expanded to support
            RSE-dependent plugins.

            Parameters
            ----------
            pfn : `str`
                A physical filename.

            Returns
            -------
            lfn : `str`
                A logical filename determined from the physical filename.
            """
            lfn = os.path.basename(pfn)
            return lfn

        # Parse metadata from LFN
        logging.debug("Parsing LFNs for metadata")
        json_meta_list = (_get_extra_json_meta(path) for path in pathlist)

        if self.rse_opts['client_domain'] == 'lan':
            force_scheme = 'file'
        else:
            force_scheme = None

        replicas = [{'scope': self.scope, 'name': _construct_lfn(path),
                     'pfn': utils.get_pfn(self.rse_info, self.scope, path, force_scheme),
                     'meta': json_meta} for path, json_meta in zip(pathlist, json_meta_list)
                    if json_meta['extension']]

        return replicas


class ReplicaRegister(RegisterClient):
    """Register a set of replicas.  This is where the action happens.

    Extends the `RegisterClient` to add registration methods.
    """

    def __init__(self, *args, container: str, framechk_opts: utils.FrameChkOpts) -> None:
        """Constructor for the ReplicaRegister class

        Parameters
        ----------
        args : Variable length argument list for `RegisterClient`.
        container : `str`
            Name (not DID) of container to attach datasets to.  Will be created if it does not
            exist.
        frame_check_opts : `utils.FrameChkOpts`
            Dictionary of parameters to use with frame validation step.
        """
        super().__init__(*args)

        self.container = container
        self.datasets = self.find_datasets()
        self.framechk_opts = framechk_opts

    @property
    def framechk_opts(self):
        """Return frame file validation options"""
        return self._framechk_opts

    @framechk_opts.setter
    def framechk_opts(self, framechk_opts: utils.FrameChkOpts) -> None:
        """
        Sets the frame validation options.

        If frame check over ssh is requested, checks ssh server is reachable.

        Parameters
        ----------
        framechk_opts: `utils.FrameChkOpts`
            Frame file validation options
        """

        # Check ssh config if requested
        if framechk_opts['hostname'] is not '' and not framechk_opts['disable']:

            # Check for keyfile
            if framechk_opts['key_filename'] is '':
                raise ValueError(f"Frame validation over ssh to {framechk_opts['hostname']} "
                                 F"requested but no keyfile provided in "
                                 F"framechk_opts['key_filename']")

            # Check keyfile exists
            if not os.path.isfile(framechk_opts['key_filename']):
                raise ValueError(
                    f"No file found for ssh keyfile at {framechk_opts['key_filename']}")

#           # Check ssh server reachable
#           policy = paramiko.AutoAddPolicy()
#
#           with paramiko.SSHClient() as ssh_client:
#
#               ssh_client.set_missing_host_key_policy(policy)
#               logging.debug("Testing SSH connection for frame validation")
#               ssh_client.connect(hostname=framechk_opts['hostname'],
#                                  username=framechk_opts['username'],
#                                  key_filename=framechk_opts['key_filename'])
#               logging.debug("SSH connection for frame validation ok")

        self._framechk_opts = framechk_opts

    @property
    def container(self):
        """Return container datasets registered in"""
        return self._container

    @container.setter
    def container(self, container: str) -> None:
        """
        Sets the container datasets will be attached to.

        Checks container exists and creates it if not.

        Parameters
        ----------
        container : `str`
            Name of container
        """

        # Check container exists and create it if not
#       try:
#           self.client.add_container(scope=self.scope, name=container, rules=[
#               {'account': self.client.account, 'copies': 1, 'rse_expression':
#                   self.rse_opts['rse'], 'grouping': 'CONTAINER', 'lifetime': None}],
#                                     meta=self.did_metadata)
#           logging.info("CONTAINER %s:%s added with rule at %s", self.scope, container,
#                        self.rse_opts['rse'])
#        except RSEWriteBlocked:
        try:
            #logging.info("Container %s:%s added, RSE write blacklisted on %s (no rule)",
            self.client.add_container(scope=self.scope, name=container, meta=self.did_metadata)
            logging.info("Container %s:%s added", self.scope, container)
        except DataIdentifierAlreadyExists:
            logging.debug("Container %s:%s already exists, ensure status=Open", self.scope,
                          container)
            set_status(scope=self.scope, name=container, open_status=True, _client=self.client)

        # Set value of dataset
        self._container = container

    @property
    def datasets(self):
        """Return the datasets list
        """
        return self._datasets

    @datasets.setter
    def datasets(self, datasets: dict):
        """"""
        self._datasets = datasets

    def find_datasets(self) -> Set[str]:
        """Enumerate datasets for files."""

        # Create dataset names
        return {get_dataset_name((replica['name'])) for replica in self.replicas}

    def add_dataset(self, dataset: str, max_tries=5, sleep_time=1) -> None:
        """
        Checks dataset exists and creates it if not.

        Parameters
        ----------
        dataset : `str`
            Name of dataset
        """
#       try:
#           self.client.add_dataset(scope=self.scope, name=dataset, rules=[
#               {'account': self.client.account, 'copies': 1, 'rse_expression':
#                   self.rse_opts['rse'], 'grouping': 'DATASET', 'lifetime': None}],
#                                   meta=self.did_metadata)
#           logging.info("DATASET %s:%s added with rule at %s", self.scope, dataset,
#                        self.rse_opts['rse'])
#       except RSEWriteBlocked:
#           logging.info("DATASET %s:%s added, RSE write blacklisted on %s (no rule)",
        try:
            self.client.add_dataset(scope=self.scope, name=dataset, meta=self.did_metadata)
            logging.info("DATASET %s:%s added", self.scope, dataset)
        except DataIdentifierAlreadyExists:
            logging.debug("DATASET %s:%s already exists", self.scope, dataset)

        container_content = [cnt['name'] for cnt in self.client.list_content(
            scope=self.scope, name=self.container) if cnt['type'] == 'DATASET']
        if dataset not in container_content:
            # Attach dataset to container
            for ntries in range(max_tries):
                # Attach datasets to containers with a jittered back-off to avoid collisions
                # between clients
                try:
                    set_status(scope=self.scope, name=self.container, open_status=True,
                               _client=self.client)
                    self.client.attach_dids(scope=self.scope, name=self.container,
                                            dids=[{'scope': self.scope, 'name': dataset}])
                    break
                except DuplicateContent as attach_err:
                    logging.debug("Dataset already attached")
                    logging.debug(attach_err)
                    break
                except UnsupportedOperation as attach_err:
                    logging.debug("Failed to attach dataset (attempt %d of %d)", ntries, max_tries)
                    logging.debug(attach_err)
                    time.sleep(sleep_time*random.uniform(0, 1))
                    sleep_time *= 2


    def add_replicas(self, logstr: str):
        """
        Add the replicas to the rucio database

        Parameters
        ----------
        logstr : `str`
            Log statement prefix (e.g. the thread number in use).
        """
        if self.replicas:
            logging.info('%s Adding replicas', logstr)

            # Why is this function here and not in add_did_meta?
            # - because we need/want the checksums when we add replicas
            def compute_metadata(this_replica: dict, client_domain: str) -> dict:
                """Retrieve metadata for an individual replica.
                Uses GFAL2 to fetch `bytes`, `adler32` and `md5` directly from the RSE using the
                file's PFN.  Modifies the input dictionary in-place.

                Parameters
                ----------
                this_replica : `dict`
                    Replica dictionary including key `pfn`.

                Returns
                -------
                this_replica : `dict`
                    Updated replica dictionary with new keys `bytes`, `adler32` and `md5`.
                """

                if client_domain == 'lan':

                    # Strip hostname, port from gfal operations
                    pfn = "file://" + urlparse(this_replica['pfn']).path
                    logging.debug("Sanitised %s -> %s for GFAL", this_replica['pfn'], pfn)
                else:
                    pfn = this_replica['pfn']

                this_replica['bytes'] = utils.gfal_bytes(pfn)
                this_replica['adler32'] = utils.gfal_adler32(pfn)
                this_replica['md5'] = utils.gfal_md5(pfn)

                return this_replica

            # DIDs already in the database:
            for rdx, replica in enumerate(self.replicas):
                logging.debug("%s Working on file %d/%d", logstr, rdx + 1, len(self.replicas))

                replica_exists = list(self.client.list_dids(scope=replica['scope'],
                                      filters={'name': replica['name'], 'type':  'file'}))

                if replica_exists:
                    logging.debug("%s Already exists, skipping checksums (%s)", logstr,
                                  replica['name'])  # DID exists
                else:
                    # DID does not exist, compute metadata and get ready to add
                    replica_start_time = time.time()

                    if not self.framechk_opts['disable']:
                        logging.debug("Validating frame content: %s %s %s",
                                      self.framechk_opts['executable'], self.framechk_opts['exe_opts'],
                                      urlparse(replica['pfn']).path)

                        sleep_time = 5
                        retries = 5
                        for ntries in range(5):
                            try:
                                _, _, validation_time = \
                                    utils.validate_frame(urlparse(replica['pfn']).path,
                                                         self.framechk_opts['executable'],
                                                         self.framechk_opts['exe_opts'],
                                                         **self.framechk_opts)
                                break

                            except utils.FrameValidationFailure as frame_exc:
                                logging.error(frame_exc)
                                if ntries == retries - 1:
                                    logging.error("Ran out of frame validation retries")
                                    raise utils.FrameValidationFailure(frame_exc)
                                logging.error("Frame validation failure (attempt %d of %d)",
                                              ntries + 1, retries)
                                logging.info("Sleeping for %d s", sleep_time)
                                time.sleep(sleep_time)
                                sleep_time *= 2

                        replica['meta']['frame-validation-time'] = validation_time

                        logging.debug("%s Validation took %.fs (%s)", logstr,
                                      time.time() - replica_start_time,
                                      urlparse(replica['pfn']).path)

                    logging.debug("%s Computing checksums for pfn %s (domain: %s)", logstr,
                                  replica['pfn'], self.rse_opts['client_domain'])
                    compute_metadata(replica, self.rse_opts['client_domain'])
                    logging.debug("%s Checksums took %.fs (%s)", logstr,
                                  time.time() - replica_start_time, replica['pfn'])

                    self.client.add_replica(rse=self.rse_opts['rse'], scope=self.scope,
                                            name=replica['name'], bytes_=replica['bytes'],
                                            adler32=replica['adler32'], pfn=replica['pfn'],
                                            md5=replica['md5'], meta=self.did_metadata)

                # Create dataset and attach file
                dataset = get_dataset_name(replica['name'])
                self.add_dataset(dataset)  # checks if it exists
                current_content = [did['name']
                                   for did in self.client.list_content(self.scope, dataset)]
                if replica['name'] not in current_content:
                    logging.info('%s Attaching %s:%s to dataset %s:%s', logstr, self.scope,
                                 replica['name'], self.scope, dataset)
                    self.client.add_files_to_dataset(scope=self.scope, name=dataset,
                                                     files=[{'scope': self.scope,
                                                             'name': replica['name']}])
                else:
                    logging.info('%s %s already attached to dataset %s', logstr, replica['name'],
                                 dataset)

                # Set DID JSON metadata
                for key in replica['meta'].keys():
                    self.client.set_metadata(scope=self.scope, name=replica['name'], key=key,
                                             value=replica['meta'][key])

                # FIXME: this would be a better place to save state, otherwise we have to get to
                #  the end of the current file list in the event of a restart.  OTOH, this would
                #  be a total nightmare with multiple threads.


def force_auth(client: Client, max_retries: int = 5, backoff_in_seconds: int = 1):
    """
    Attempt an authorized operation (rucio whoami) up to `max_retries` times and a backoff to
    ensure existence of valid internal authentication token.

    Parameters
    ----------
    client : `rucio.client.client.Client`
        Rucio client instance.  Will be created if needed.
    max_retries: `int`
        Maximum number of times to try rucio whoami
    backoff_in_seconds: `int`
        Base number of seconds for randomised backoff interval between attempts
    """
    # Generate internal authorization token
    auth_retry = 0
    while True:
        try:
            logging.debug("Attempting client.whoami() for authentication")
            return client.whoami()
        except CannotAuthenticate as no_auth:
            logging.critical(no_auth)
            logging.critical("Whoami authentication failed, retrying %s/%s", auth_retry + 1,
                             max_retries)
            if auth_retry == max_retries - 1:
                raise CannotAuthenticate(no_auth)
            else:
                sleep = (backoff_in_seconds * 2 ** auth_retry +
                         random.uniform(0, 1))
                time.sleep(sleep)
                auth_retry += 1
        except ConnectionError as cnc_err:
            logging.critical(cnc_err)
            logging.critical("Connection error, retrying %s/%s", auth_retry + 1, max_retries)
            if auth_retry == max_retries - 1:
                raise CannotAuthenticate(cnc_err)
            else:
                sleep = (backoff_in_seconds * 2 ** auth_retry +
                         random.uniform(0, 1))
                time.sleep(sleep)
                auth_retry += 1


def register_wrapper(did_set: ReplicaRegister = None, thread_info: Tuple[int, int] = None) -> None:
    """Helper function to call methods on RegisterReplica instance to.  Function
    must use keyword arguments for threading.  Placing operations inside function to support
    multithread operations.

    Parameters
    ----------
    did_set : `ReplicaRegister`
        A set of replicas to register.  For threaded operations, this is a subset of the whole.
    thread_info : `tuple(int, int)`
        The thread index and thread count for multithread operations.
    """

    # Logging prefix
    prepend_str = f'Thread [{thread_info[0]}/{thread_info[1]}] :'

    # Add files to database
    try:
        did_set.add_replicas(prepend_str)
    except Exception as exc:
        logging.exception(exc)
        # FIXME: we should really be handling this better
        os._exit(1)


def register(rse_opts: utils.RSEOpts, container_did: str, diskcache_params: utils.DiskCacheParams,
             framechk_opts: utils.FrameChkOpts,
             did_metadata=None, exec_opts: utils.ExecOpts = None):
    """Register files, create and attach to datasets and a parent container.  Also appends
    metadata with exception handling.

    Principal operations:

    1. Identify input files
    2. Parse file metadata and create collections of DIDs
    3. Compute file metadata and register for each set of DIDs

    Parameters
    ----------
    rse_opts : `utils.RSEOpts`
        Name and scheme to use for RSE
    container_did : `str`
        Rucio data identifier for container to which we are adding datasets.  Will be created if it
        does not exist.  Must be a DID: `scope:name`.
    diskcache_params : `utils.FrameChkParams`
        Dictionary of parameters to use to parse the LDAS DiskCache ASCII dump.
    framechk_opts : `utils.FrameChkOpts`
        Dictionary of parameters to use with frame validation step.
    did_metadata : `dict`
        Optional dictionary of key-value pairs which will be attached to all DIDs in this dataset.
    exec_opts : `utils.ExecOpts`
        Dictionary of parameters to manage the registration process.

    """
    # FIXME: use c.add_value(key='datatype', value=value) to ensure key is available

    # Start timer
    global_start_time = time.time()

    # Get scope and name of target dataset
    scope, container = extract_scope(container_did)
    if scope == container:
        raise ValueError("Container DID should follow scope:name")

    if not exec_opts:
        exec_opts = {'nthreads': 1, 'sleep_interval': 30, 'online': False, 'resume_from_db': False}

    n_iterations = 0
    while not GRACEFUL_STOP.is_set():
        local_start_time = time.time()

        # Instantiate client
        client = Client()

        # Make sure we have a valid auth token
        whoiam = force_auth(client)
        logging.debug(whoiam)

        # Read the minimuum-gps from the state file
        minimum_gps = diskcache_params['minimum_gps']
        state_file = f'{get_state_dir()}/gravcat-state-{scope}:{container}'
        if exec_opts['online'] and exec_opts['save_state']:

            logging.debug("Attempting to read state from %s", state_file)
            try:
                with open(state_file, 'r', encoding='UTF-8') as statef:
                    minimum_gps = int(statef.read())
                logging.debug('Using minimum-gps from state file: %d', minimum_gps)
            except FileNotFoundError:
                minimum_gps = diskcache_params['minimum_gps']
                logging.debug('Using minimum-gps from original inputs: %d', minimum_gps)

        # Get list of all pre-registered and attached files
        file_paths = utils.files_to_register(scope, diskcache_params['path'],
                                             diskcache_params['regexp'],
                                             minimum_gps=minimum_gps,
                                             maximum_gps=diskcache_params['maximum_gps'],
                                             extension=diskcache_params['extension'])

        if len(file_paths) > 0:

            # Ensure we only have useful threads
            if exec_opts['nthreads'] > len(file_paths):
                nthreads = len(file_paths)
            else:
                nthreads = exec_opts['nthreads']

            logging.debug('Starting %d registration threads', nthreads)

            # Generate replica sets for each thread
            did_sets = (ReplicaRegister(file_group, scope, rse_opts, did_metadata,
                                        client, container=container,
                                        framechk_opts=framechk_opts) for file_group in
                        utils.grouper(file_paths, nthreads))

            # Initialise threads
            threads = list()
            for idx, did_set in enumerate(did_sets):

                logging.debug("Setting up thread %d of %d", idx, nthreads)

                kwargs = {'did_set': did_set,
                          'thread_info': (idx + 1, nthreads)}

                threads.append(threading.Thread(target=register_wrapper, kwargs=kwargs))

            # Start threads
            logging.debug("Starting num. active threads: %d", threading.active_count())
            logging.debug("Starting threads")
            _ = [thread.start() for thread in threads]

            # Wait for threads to finish
            while threads[0].is_alive():
                logging.debug('Still %i active threads', len(threads))
                try:
                    _ = [thread.join() for thread in threads if thread and thread.is_alive()]
                except Exception as e:
                    print("Exception Handled in Main, Details of the Exception:", e)

            logging.debug("Ending num. active threads: %d", threading.active_count())
            logging.debug("Threads complete")

            # Close containers and datasets here, after threads have finished
            logging.debug("Closing container and datasets")
            set_status(scope, name=container, open_status=False, _client=client)

            for dataset in set().union(*[did_set.datasets for did_set in did_sets]):
                set_status(scope, name=dataset, open_status=False, _client=client)

            # Write out gps end time of last file
            gps_end_time = utils.gps_end_of_list(file_paths)
            if exec_opts['online'] and exec_opts['save_state']:
                with open(state_file, 'w', encoding='UTF-8') as statef:
                    statef.write(str(gps_end_time))

            # Update minimum-gps for next loop
            diskcache_params['minimum_gps'] = gps_end_time

        del client

        # Timing info
        n_iterations += 1
        logging.info("%d iterations so far", n_iterations)
        logging.info("This iteration took: %-0.4f sec.", (time.time() - local_start_time))
        logging.info("Total uptime: %-0.4f sec.", time.time() - global_start_time)

        # Stop the loop unless we're running in online mode
        if not exec_opts['online']:
            logging.info("Not running with --online, exiting.")
            break

        daemon_sleep(start_time=local_start_time, sleep_time=exec_opts['sleep_interval'],
                     graceful_stop=GRACEFUL_STOP)

    logging.info("Graceful stop requested.")
    logging.info("Graceful stop done.")

    return 0
