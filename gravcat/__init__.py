# -*- coding: utf-8 -*-
# Copyright James Alexander Clark 2021
#
# This file is part of GravCat.
#
# GWRUcio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# GWDataFind is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""The client library for the LIGO bulk data registration in Rucio.
"""

__author__ = 'James Alexander Clark <james.clark@ligo.org>'
__version__ = '2.0.0'
