# -*- coding: utf-8 -*-
# Copyright (C) 2021  James Alexander Clark <james.clark@ligo.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Utility to register LIGO/Virgo datasets into rucio.

Data may be registered as individual files, ascii lists of files, or registered
on the fly as a background process monitoring a DiskCacheFile.
"""

import argparse
import configparser
import os
import sys

import argcomplete

from rucio.common.utils import detect_client_location

from . import __version__
from . import workflow
from .registration import (register, stop)
from . import utils

__author__ = 'James Alexander Clark <james.clark@ligo.org>'


class EnvInterpolation(configparser.BasicInterpolation):
    """Interpolation which expands environment variables in values."""

    def before_get(self, parser, section, option, value, defaults):
        value = super().before_get(parser, section, option, value, defaults)
        return os.path.expandvars(value)

def parse_inputs():
    """Command line parser
    """

    aparser = argparse.ArgumentParser(description=__doc__)

    # Common parser options
    aparser.add_argument('-c',
                         "--config-file",
                         metavar="FILE",
                         type=argparse.FileType('r', encoding='UTF-8'),
                         required=True,
                         help="""Python configuration (ini) file to describe dataset."""
                         )

    aparser.add_argument('-d',
                         "--client-domain",
                         type=str,
                         default=['lan' if
                                  detect_client_location()['site'] ==
                                  os.environ.get('SITE_NAME') else 'wan'][0],
                         help="""Protocol domain to read from RSE.  Set to 'lan' or 
                                 'wan', defaults to auto-detected from environment variable 
                                 SITE_NAME""")

    aparser.add_argument('-v', '--version', action='version',
                         version=__version__,
                         help="""show version number and exit""")

    aparser.add_argument('-r',
                         "--rse",
                         type=str,
                         required=True,
                         help="""RSE to register replicas at""")

    aparser.add_argument('-p',
                         "--pfns-file",
                         metavar="FILE",
                         type=argparse.FileType('r', encoding='UTF-8'),
                         required=True,
                         help="""Path to diskcache ascii dump or list of PFNs""")

    aparser.add_argument('-s',
                         "--minimum-gps",
                         type=int,
                         default=None,
                         required=False,
                         help="""Min GPS time to register files from
                         (overrides values in config file)""")

    aparser.add_argument('-e',
                         "--maximum-gps",
                         type=int,
                         default=None,
                         required=False,
                         help="""Max GPS time to register files from
                         (overrides values in config file)""")

    # Operation-specific parsers
    subparsers = aparser.add_subparsers()

    # Gap-check parser: options for checking for missing LFNs
    gaps_parser = subparsers.add_parser(
        'fill-gaps',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        help="Check for gaps in the rucio catalog and create an HTCondor workflow to plug them",
        epilog="""Usage example
^^^^^^^^^^^^^

To check for missing files in the rucio catalog relative to an LDAS diskcache dump or a list 
of PFNs:

$ gravcat --config-file l1-raw.ini --pfns-file /data/diskcache.ldas fill-gaps

where  l1-raw.ini is a config file file like:

```
[catalog]
scope=LIGO.frames.postO3
container=L-L1_R

[diskcache-params]
regexp=L1_R-
extension=gwf # optional
minimum-gps=1351541799
maximum-gps=1357063808

[common-metadata]
purpose=gravcat-dev
```
            """)

    gaps_parser.set_defaults(which='fill-gaps')

    gaps_parser.add_argument('-n',
                             "--n-pfns-per-job",
                             type=int,
                             default=0,
                             required=False,
                             help="""Number of PFNs per regitration job.  If set or left at default
                             of zero, will attempt to register any missing PFNs in a single job.""")

    # Registration parser: options for registration in rucio catalog
    register_parser = subparsers.add_parser(
        'register',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        help="Register frame files in Rucio catalog.",
        epilog="""Usage example
^^^^^^^^^^^^^

To register files from an LDAS diskcache ASCII dump OR from a file with a list of PFNs:

$ gravcat --config-file l1-raw.ini --pfns-file /data/diskcache.ldas --rse LIGO-LA register 

where  l1-raw.ini is a config file file like:

```
[catalog]
scope=LIGO.frames.postO3
container=L-L1_R

[diskcache-params]
regexp=L1_R-
extension=gwf # optional
minimum-gps=1351541799
maximum-gps=1357063808

[common-metadata]
purpose=gravcat-dev
```
        """)

    register_parser.set_defaults(which='register')

    register_parser.add_argument("--online",
                                 default=False,
                                 action="store_true",
                                 help="""Run until interrupted""")

    register_parser.add_argument("--save-state",
                                 default=False,
                                 action="store_true",
                                 help="""Record the time of the last registered file""")

    register_parser.add_argument("--disable-framechk",
                                 default=False,
                                 action="store_true",
                                 help="""Disable frame file verification step""")

    register_parser.add_argument('-S',
                                 "--sleep-interval",
                                 type=float,
                                 default=5,
                                 required=False,
                                 help="""Number of seconds to sleep between
                         registration loops when running online""")

    register_parser.add_argument('-t',
                                 "--threads",
                                 type=int,
                                 default=1,
                                 help="""Number of worker threads""")

    argcomplete.autocomplete(aparser)

    aparser = aparser.parse_args(sys.argv[1:])
    aparser.config_file.close()
    aparser.pfns_file.close()

    # Parse configfile for datasets description
    cparser = configparser.ConfigParser(interpolation=EnvInterpolation())

    # cparser.optionxform = str
    cparser.read([aparser.config_file.name])

    # Command line options for times override config file
    if aparser.minimum_gps:
        cparser.set('diskcache-params', 'minimum-gps', str(aparser.minimum_gps))

    if aparser.maximum_gps:
        cparser.set('diskcache-params', 'maximum-gps', str(aparser.maximum_gps))

    return aparser, cparser


def main():
    """Run the thing
    """

    # Parse input
    args, config = parse_inputs()

    # Extract common options

    # Config file
    scope = config.get('catalog', 'scope')
    container = config.get('catalog', 'container')

    # Dictionary to describe dataset
    disk_cache_params = utils.DiskCacheParams(path=args.pfns_file.name,
                                              minimum_gps=config.getint('diskcache-params',
                                                                        'minimum-gps', fallback=0),
                                              maximum_gps=config.getint('diskcache-params',
                                                                        'maximum-gps',
                                                                        fallback=2000000000),
                                              regexp=config.get('diskcache-params', 'regexp'),
                                              extension=config.get('disk-cache-params', 'extension',
                                                                   fallback='gwf')
                                              )

    if args.which == 'register':


        # Dictionary of metadata to attach to all newly registered DIDs
        try:
            did_metadata = dict(config.items('common-metadata'))
        except configparser.NoSectionError:
            did_metadata = None

        framechk_opts = utils.FrameChkOpts(disable=args.disable_framechk,
                                           executable=config.get('frame-validation', 'executable',
                                                                 fallback='/cvmfs/oasis.opensciencegrid.org'
                                                                 '/ligo/sw/conda/envs/igwn/bin/'
                                                                 'framecpp_verify'),
                                           exe_opts=config.get('frame-validation', 'exe-opts',
                                                               fallback=''),
                                           username=config.get('frame-validation', 'ssh-user',
                                                               fallback='rucio'),
                                           hostname=config.get('frame-validation', 'ssh-host',
                                                               fallback=''),
                                           key_filename=config.get('frame-validation',
                                                                   'ssh-keyfile', fallback=''),
                                           timeout=config.getint('frame-validation', 'ssh-timeout',
                                                                 fallback=120),
                                           banner_timeout=config.getint('frame-validation',
                                                                        'ssh-banner_timeout',
                                                                        fallback=120),
                                           auth_timeout=config.getint('frame-validation',
                                                                      'ssh-auth_timeout',
                                                                      fallback=120),
                                           channel_timeout=config.getint('frame-validation',
                                                                         'ssh-channel_timeout',
                                                                         fallback=120),
                                           exec_timeout=config.getint('frame-validation',
                                                                      'ssh-exec_timeout',
                                                                      fallback=300),
                                           retries=config.getint('frame-validation',
                                                                 'ssh-retries', fallback=5),
                                           sleep_time=config.getint('frame-validation',
                                                                    'ssh-sleep_time', fallback=5),
                                           )

        if 'lan' == args.client_domain:
            # Override any request for ssh access
            framechk_opts['hostname'] = ''

        if (args.client_domain == 'wan' and framechk_opts['hostname'] == '') and not \
                args.disable_framechk:
            raise NotImplementedError("Client domain set to WAN, frame validation enabled but no "
                                      "ssh server for frame validation specified in "
                                      "ssh-host in config file.")

        # Dictionary to describe execution options
        exec_opts = utils.ExecOpts(nthreads=args.threads, sleep_interval=args.sleep_interval,
                                   online=args.online, save_state=args.save_state)

        #rse_opts = {'rse': args.rse, 'client_domain': args.client_domain}
        rse_opts = utils.RSEOpts(rse=args.rse, client_domain=args.client_domain)


        try:
            register(rse_opts, f'{scope}:{container}', disk_cache_params, framechk_opts,
                     did_metadata, exec_opts)
        except KeyboardInterrupt:
            stop()

    elif 'fill-gaps' == args.which:

        #  Get lists of missing files
        missing_files = workflow.check_gaps(scope, disk_cache_params, args.n_pfns_per_job)

        if missing_files:
            if 'lan' == args.client_domain:
                raise NotImplementedError("Client domain set to WAN, but HTCondor deployment requested")

            if config.has_section('htcondor'):
                htcondor_overrides = dict(config['htcondor'])
            else:
                htcondor_overrides = {}

            # Generate DAGMan workflow
            workflow.make_register_dag(file_lists=missing_files, config_file=args.config_file.name,
                                      rse=args.rse, htcondor_overrides=htcondor_overrides)


if __name__ == "__main__":
    sys.exit(main())
