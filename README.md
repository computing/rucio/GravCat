# GravCat

Tools for registering local or remote gravitational wave data in a Rucio
catalog.

Supported data:
 * Frame files
 * SFT files
